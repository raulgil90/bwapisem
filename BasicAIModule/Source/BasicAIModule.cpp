#include "BasicAIModule.h"
#include "../Addons/Util.h"
#include <vector>
#include <list>
#include <map>
#include <string>
#include <math.h>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <time.h>

using namespace BWAPI;
using namespace std;
list<string> units_list;
list<int> times;
vector<int> seenstuff;
time_t t;
time_t gamestart;
time_t gameend;
std::set<Unit*> zealotsAttacking;
std::set<Unit*>::iterator zealotAtkItr;
std::set<Unit*> zealotHasTarget;

struct PointStruct {
	int x_coor;
	int y_coor;
	int dist_from_base;
};


void BasicAIModule::onStart()
{
  gamestart=time(NULL);
  Broodwar->setLocalSpeed(1);
  Broodwar->printf("LOADED AI BOT FILE...\n");
  this->showManagerAssignments=false;
  if (Broodwar->isReplay()) return;
  // Enable some cheat flags
  Broodwar->enableFlag(Flag::UserInput);
  //Broodwar->enableFlag(Flag::CompleteMapInformation);
  BWTA::readMap();
  BWTA::analyze();
  this->analyzed=true;
  this->buildManager       = new BuildManager(&this->arbitrator);
  this->techManager        = new TechManager(&this->arbitrator);
  this->upgradeManager     = new UpgradeManager(&this->arbitrator);
  this->scoutManager       = new ScoutManager(&this->arbitrator);
  this->workerManager      = new WorkerManager(&this->arbitrator);
  this->supplyManager      = new SupplyManager();
  this->baseManager        = new BaseManager();
  this->buildOrderManager  = new BuildOrderManager(this->buildManager,this->techManager,this->upgradeManager,this->workerManager,this->supplyManager);
  this->defenseManager     = new DefenseManager(&this->arbitrator);
  this->informationManager = InformationManager::create();
  this->borderManager      = BorderManager::create();
  this->unitGroupManager   = UnitGroupManager::create();
  this->enhancedUI         = new EnhancedUI();

  this->supplyManager->setBuildManager(this->buildManager);
  this->supplyManager->setBuildOrderManager(this->buildOrderManager);
  this->techManager->setBuildingPlacer(this->buildManager->getBuildingPlacer());
  this->upgradeManager->setBuildingPlacer(this->buildManager->getBuildingPlacer());
  this->workerManager->setBaseManager(this->baseManager);
  this->workerManager->setBuildOrderManager(this->buildOrderManager);
  this->baseManager->setBuildOrderManager(this->buildOrderManager);
  this->baseManager->setBorderManager(this->borderManager);
  this->defenseManager->setBorderManager(this->borderManager);
  //testcomment
  BWAPI::Race race = Broodwar->self()->getRace();
  BWAPI::Race enemyRace = Broodwar->enemy()->getRace();
  BWAPI::UnitType workerType=race.getWorker();
  double minDist;
  BWTA::BaseLocation* natural=NULL;
  BWTA::BaseLocation* home=BWTA::getStartLocation(Broodwar->self());
  for(std::set<BWTA::BaseLocation*>::const_iterator b=BWTA::getBaseLocations().begin();b!=BWTA::getBaseLocations().end();b++)
  {
    if (*b==home) continue;
    double dist=home->getGroundDistance(*b);
    if (dist>0)
    {
      if (natural==NULL || dist<minDist)
      {
        minDist=dist;
        natural=*b;
      }
    }
  }
  this->buildOrderManager->enableDependencyResolver();
  //make the basic production facility
  if (race == Races::Zerg)
  {
    //send an overlord out if Zerg
    this->scoutManager->setScoutCount(1);

    //9 pool
    /*
    this->buildOrderManager->build(9,workerType,80);
    this->buildOrderManager->buildAdditional(3,UnitTypes::Zerg_Zergling,82);
    this->buildOrderManager->buildAdditional(1,UnitTypes::Zerg_Spawning_Pool,79);
    this->buildOrderManager->build(40,workerType,78);
    */
    //12 hatch
    this->buildOrderManager->build(9,UnitTypes::Zerg_Drone,82);
    this->buildOrderManager->build(2,UnitTypes::Zerg_Overlord,81);
    this->buildOrderManager->build(12,UnitTypes::Zerg_Drone,80);
    this->baseManager->expand(natural,79);
    this->buildOrderManager->build(1,UnitTypes::Zerg_Extractor,78);
    this->buildOrderManager->build(20,UnitTypes::Zerg_Drone,77);
    this->buildOrderManager->build(60,UnitTypes::Zerg_Drone,75);
    this->buildOrderManager->build(1,UnitTypes::Zerg_Spawning_Pool,60);
    this->buildOrderManager->build(3,UnitTypes::Zerg_Zergling,82);
    this->buildOrderManager->build(1,UnitTypes::Zerg_Lair,55);
    this->buildOrderManager->buildAdditional(1,UnitTypes::Zerg_Hatchery,50);
    this->buildOrderManager->build(1,UnitTypes::Zerg_Spire,45);
    this->buildOrderManager->build(9,UnitTypes::Zerg_Mutalisk,120);
  }
  else if (race == Races::Terran)
  {
    this->buildOrderManager->build(20,workerType,80);
    if (enemyRace == Races::Zerg)
    {
      this->buildOrderManager->buildAdditional(1,UnitTypes::Terran_Barracks,60);
      this->buildOrderManager->buildAdditional(9,UnitTypes::Terran_Marine,45);
      this->buildOrderManager->buildAdditional(1,UnitTypes::Terran_Refinery,42);
      this->buildOrderManager->buildAdditional(1,UnitTypes::Terran_Barracks,40);
      this->buildOrderManager->buildAdditional(1,UnitTypes::Terran_Academy,39);
      this->buildOrderManager->buildAdditional(9,UnitTypes::Terran_Medic,38);
      this->buildOrderManager->research(TechTypes::Stim_Packs,35);
      this->buildOrderManager->research(TechTypes::Tank_Siege_Mode,35);
      this->buildOrderManager->buildAdditional(3,UnitTypes::Terran_Siege_Tank_Tank_Mode,34);
      this->buildOrderManager->buildAdditional(2,UnitTypes::Terran_Science_Vessel,30);
      this->buildOrderManager->research(TechTypes::Irradiate,30);
      this->buildOrderManager->upgrade(1,UpgradeTypes::Terran_Infantry_Weapons,20);
      this->buildOrderManager->build(3,UnitTypes::Terran_Missile_Turret,13);
      this->buildOrderManager->upgrade(3,UpgradeTypes::Terran_Infantry_Weapons,12);
      this->buildOrderManager->upgrade(3,UpgradeTypes::Terran_Infantry_Armor,12);
      this->buildOrderManager->build(1,UnitTypes::Terran_Engineering_Bay,11);
      this->buildOrderManager->buildAdditional(40,UnitTypes::Terran_Marine,10);
      this->buildOrderManager->build(6,UnitTypes::Terran_Barracks,8);
      this->buildOrderManager->build(2,UnitTypes::Terran_Engineering_Bay,7);
      this->buildOrderManager->buildAdditional(10,UnitTypes::Terran_Siege_Tank_Tank_Mode,5);
    }
    else
    {
      this->buildOrderManager->buildAdditional(2,BWAPI::UnitTypes::Terran_Machine_Shop,70);
      this->buildOrderManager->buildAdditional(3,BWAPI::UnitTypes::Terran_Factory,60);
      this->buildOrderManager->research(TechTypes::Spider_Mines,55);
      this->buildOrderManager->research(TechTypes::Tank_Siege_Mode,55);
      this->buildOrderManager->buildAdditional(20,BWAPI::UnitTypes::Terran_Vulture,40);
      this->buildOrderManager->buildAdditional(20,BWAPI::UnitTypes::Terran_Siege_Tank_Tank_Mode,40);
      this->buildOrderManager->upgrade(3,UpgradeTypes::Terran_Vehicle_Weapons,20);
    }
  }
  else if (race == Races::Protoss)
  {
    this->buildOrderManager->build(10,workerType,81);
    this->buildOrderManager->build(60,UnitTypes::Protoss_Zealot,79);
	this->buildOrderManager->build(3,UnitTypes::Protoss_Gateway,80);
  }
  this->workerManager->enableAutoBuild();
  this->workerManager->setAutoBuildPriority(40);
 // this->baseManager->setRefineryBuildPriority(30);

}

BasicAIModule::~BasicAIModule()
{
  delete this->buildManager;
  delete this->techManager;
  delete this->upgradeManager;
  delete this->scoutManager;
  delete this->workerManager;
  delete this->supplyManager;
  delete this->buildOrderManager;
  delete this->baseManager;
  delete this->defenseManager;
  InformationManager::destroy();
  BorderManager::destroy();
  UnitGroupManager::destroy();
  delete this->enhancedUI;
}
void BasicAIModule::onEnd(bool isWinner)
{
  /****************************************************************/
  gameend = time(NULL);
  ofstream myfile;
  stringstream ss;
  t = time(NULL);
  string filename;
  ss << t;
  filename = ss.str();
  myfile.open (filename.c_str());
  list<string>::iterator units_it;
  list<int>::iterator times_it;
  times_it = times.begin();
  for (units_it=units_list.begin(); units_it!=units_list.end(); units_it++)
  {
	  myfile << *units_it << " ";
	  int times = *times_it;
	  int minutes = times/60;
      int seconds = times%60;
	  myfile << minutes << " mins " << seconds << " seconds"<<endl;
	  times_it++;
  }
  myfile.close(); 
  /****************************************************************/
  log("onEnd(%d)\n",isWinner);
}

vector<PointStruct> pStructVec;					// vector to pStruct
vector<PointStruct>::iterator psvecItr;
int calc_distance(PointStruct *PS, int base_x, int base_y);
int calc_distance(PointStruct *PS, int base_x, int base_y) {

	int x = PS->x_coor - base_x;	// (X2 - X1)
	int y = PS->y_coor - base_y;	// (Y2 - Y1)

	x = static_cast<int>(pow((float)x,2));		// x^2
	y = static_cast<int>(pow((float)y,2));		// y^2

	return static_cast<int>(sqrt(static_cast<float>(x+y)));

}// calculate the distance using the distance formula

bool dist_sorter(PointStruct const& P1, PointStruct const& P2) {
	return (P1.dist_from_base < P2.dist_from_base);
}
int j =0;
int enemyX=0;
int enemyY=0;

bool didPrint = false;
int baseX;
int baseY;
bool gotBase = false;


void BasicAIModule::onFrame()
{
  if (Broodwar->isReplay()) return;
  if (!this->analyzed) return;
  this->buildManager->update();
  this->buildOrderManager->update();
  this->baseManager->update();
  this->workerManager->update();
  this->techManager->update();
  this->upgradeManager->update();
  this->supplyManager->update();
  this->scoutManager->update();
  this->enhancedUI->update();
  this->borderManager->update();
  this->defenseManager->update();
  this->arbitrator.update();
  // ---------------- Get all the choke points on the map ---------------------- //
  set<BWTA::Chokepoint*> chokePts = BWTA::getChokepoints();
  set<BWTA::Chokepoint*>::iterator cItr;
  // --------------------------------------------------------------------------- // 

  // --------------- Get all of the units ------------------- //
  std::set<Unit*> units=Broodwar->self()->getUnits();
  set<Unit*>::iterator unitItr;

  std::set<Unit*> zealotsNotAttacking;
  std::set<Unit*>::iterator zealotItr;

  std::set<BWTA::Chokepoint*> enemyBorder =  this->borderManager->getEnemyBorder();
  std::set<BWTA::Chokepoint*>::iterator enemyBorItr;

  
  // -------------------------------------------------------- //
  if(Broodwar->getFrameCount() < 30) {
	  this->scoutManager->setScoutCount(1);
	  		for(unitItr=units.begin();unitItr!=units.end() && gotBase == false;unitItr++){
			if((*unitItr)->getType() == BWAPI::UnitTypes::Protoss_Nexus){
				baseX = (*unitItr)->getPosition().x();
				baseY = (*unitItr)->getPosition().y();
				gotBase=true;
			}
		}
		
		for(cItr = chokePts.begin(); cItr != chokePts.end() && didPrint == false; cItr++) {
			PointStruct *psPtr = new PointStruct;
			psPtr->x_coor = static_cast<int>((*cItr)->getCenter().x());
			psPtr->y_coor = static_cast<int>((*cItr)->getCenter().y());
			psPtr->dist_from_base = calc_distance(psPtr,baseX,baseY);
			pStructVec.push_back(*psPtr);
		}// for all of the choke points, store these locations in the vector
		sort(pStructVec.begin(), pStructVec.end(), &dist_sorter);
		
		didPrint = true;
		for(enemyBorItr = enemyBorder.begin(); enemyBorItr != enemyBorder.end(); enemyBorItr++) {
			enemyX = (*enemyBorItr)->getCenter().x();
			enemyY = (*enemyBorItr)->getCenter().y();
		}
  }

  int numOfZealots = 0;
  for(unitItr=units.begin(); unitItr != units.end(); unitItr++) {
	  if((*unitItr)->getType() == BWAPI::UnitTypes::Protoss_Zealot ) {
		  if((*unitItr)->isIdle()) {
			  numOfZealots++;
			  zealotsNotAttacking.insert((*unitItr));			// add to non attacking zealots
		  }
	  }
  }

  if(Broodwar->getFrameCount() % 500 < 1) {
	  Broodwar->printf("Zealots not attacking: %ld\n", numOfZealots);
	  Broodwar->printf("Zealots attacking: %ld\n", zealotsAttacking.size());
  }

  UnitGroup enemyunits = SelectAllEnemy();
	if(numOfZealots >= 10 ) {
		for(zealotItr = zealotsNotAttacking.begin(); zealotItr != zealotsNotAttacking.end() && enemyX > 0 && enemyY > 0; zealotItr++) {
			// instruct the zealots to move to last chokepoint
			(*zealotItr)->attack(Position::Position(enemyX, enemyY));
			zealotsAttacking.insert((*zealotItr));
			zealotsNotAttacking.erase((*zealotItr));
		}// for all the zealots not attacking, as long as the enemy bases have been calculated, send 8 zealots to attack the enemy base
		numOfZealots = 0;								// start over again
	}

	if (this->showManagerAssignments)
	{
		for(std::set<Unit*>::iterator i=units.begin();i!=units.end();i++)
		{
			if (this->arbitrator.hasBid(*i))
			{
				int x=(*i)->getPosition().x();
				int y=(*i)->getPosition().y();
		
				std::list< std::pair< Arbitrator::Controller<BWAPI::Unit*,double>*, double> > bids=this->arbitrator.getAllBidders(*i);
				int y_off=0;
				bool first = false;
				const char activeColor = '\x07', inactiveColor = '\x16';
				char color = activeColor;
				for(std::list< std::pair< Arbitrator::Controller<BWAPI::Unit*,double>*, double> >::iterator j=bids.begin();j!=bids.end();j++)
				{
					Broodwar->drawTextMap(x,y+y_off,"%c%s: %d",color,j->first->getShortName().c_str(),(int)j->second);
					y_off+=15;
					color = inactiveColor;
				}
			}
		}
	}

	UnitGroup myPylonsAndGateways = SelectAll()(Pylon,Gateway)(HitPoints,"<=",200);
	for each(Unit* u in myPylonsAndGateways)
	{
		Broodwar->drawCircleMap(u->getPosition().x(),u->getPosition().y(),20,Colors::Red);
	}

  /****************************************************************/
	string unit_id;
	int found = 0;
	enemyunits = SelectAllEnemy();
	for each(Unit* u in enemyunits)
	{
		UnitType u_type =  u->getType();
		if(u_type == BWAPI::UnitTypes::Zerg_Hatchery || u_type == BWAPI::UnitTypes::Terran_Command_Center || u_type == BWAPI::UnitTypes::Protoss_Nexus) {
		 // do something
			 enemyX = u->getPosition().x();
			enemyY = u->getPosition().y();
		}// if we see any enemy bases, record enemy base locations
 
		string name = u_type.getName();
		int unit_id;
		unit_id = u->getID();
		vector<int>::iterator my_it;
	  /*search for the item found*/
		for ( my_it=seenstuff.begin() ; my_it < seenstuff.end(); my_it++ )
		{
			if (*my_it == unit_id)
			{
				found = 1;
			}
		}
     
		if (found == 0)
		{
			 Broodwar->printf("new item found: %i of type %s \n",unit_id, name.c_str());
			 time_t now;
			 now = time(NULL);
			 time_t gametime; 
			 gametime = now - gamestart;
			 units_list.push_back (name);
			 times.push_back ((int)gametime);
			 seenstuff.push_back(unit_id);
		}
		found = 0;

	}
  /***************************************************************/
}
	
void BasicAIModule::onUnitDestroy(BWAPI::Unit* unit)
{
  if (Broodwar->isReplay()) return;
  this->arbitrator.onRemoveObject(unit);
  this->buildManager->onRemoveUnit(unit);
  this->techManager->onRemoveUnit(unit);
  this->upgradeManager->onRemoveUnit(unit);
  this->workerManager->onRemoveUnit(unit);
  this->scoutManager->onRemoveUnit(unit);
  this->defenseManager->onRemoveUnit(unit);
  this->informationManager->onUnitDestroy(unit);
  this->baseManager->onRemoveUnit(unit);
}

void BasicAIModule::onUnitDiscover(BWAPI::Unit* unit)
{
  if (Broodwar->isReplay()) return;
  this->informationManager->onUnitDiscover(unit);
  this->unitGroupManager->onUnitDiscover(unit);
}
void BasicAIModule::onUnitEvade(BWAPI::Unit* unit)
{
  if (Broodwar->isReplay()) return;
  this->informationManager->onUnitEvade(unit);
  this->unitGroupManager->onUnitEvade(unit);
}

void BasicAIModule::onUnitMorph(BWAPI::Unit* unit)
{
  if (Broodwar->isReplay()) return;
  this->unitGroupManager->onUnitMorph(unit);
}
void BasicAIModule::onUnitRenegade(BWAPI::Unit* unit)
{
  if (Broodwar->isReplay()) return;
  this->unitGroupManager->onUnitRenegade(unit);
}

void BasicAIModule::onSendText(std::string text)
{
  if (Broodwar->isReplay())
  {
    Broodwar->sendText("%s",text.c_str());
    return;
  }
  UnitType type=UnitTypes::getUnitType(text);
  if (text=="debug")
  {
    if (this->showManagerAssignments==false)
    {
      this->showManagerAssignments=true;
      this->buildOrderManager->setDebugMode(true);
      this->scoutManager->setDebugMode(true);
    }
    else
    {
      this->showManagerAssignments=false;
      this->buildOrderManager->setDebugMode(false);
      this->scoutManager->setDebugMode(false);
    }
    Broodwar->printf("%s",text.c_str());
    return;
  }
  if (text=="expand")
  {
    this->baseManager->expand();
    Broodwar->printf("%s",text.c_str());
    return;
  }
  if (type!=UnitTypes::Unknown)
  {
    this->buildOrderManager->buildAdditional(1,type,300);
  }
  else
  {
    TechType type=TechTypes::getTechType(text);
    if (type!=TechTypes::Unknown)
    {
      this->techManager->research(type);
    }
    else
    {
      UpgradeType type=UpgradeTypes::getUpgradeType(text);
      if (type!=UpgradeTypes::Unknown)
      {
        this->upgradeManager->upgrade(type);
      }
      else
        Broodwar->printf("You typed '%s'!",text.c_str());
    }
  }
  Broodwar->sendText("%s",text.c_str());
}
